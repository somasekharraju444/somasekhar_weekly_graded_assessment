package bookstore;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
	    MagicOfBooks mb=new MagicOfBooks();
	   
		while (true) {
			System.out.println("1. Add a new Book");
			System.out.println("2. Delete a Book");
			System.out.println("3. Update a Book");
			System.out.println("4. Display all the Books");
			System.out.println("5. See the total count of the Books");
			System.out.println("6. See all the Books under Autobiography genre");
			System.out.println("7. To Arrange the Books in following order : ");
			
			System.out.println("enter your choice:");
			int choice = sc.nextInt();

			switch (choice) {

			case 1:
				System.out.println("Please Enter Number of Books you want to add");
				int n=sc.nextInt();
				for(int i=1;i<=n;i++) {
					mb.addbook();
				}
				break;
			
			case 2:
				mb.deletebook();
				break;
	
			case 3:
				mb.updatebook();
				break;

			case 4:
				mb.displayBookInfo();
				break;
			
			case 5:
				System.out.println("Count of all books-");
				mb.count();
				break;
				
			case 6:
				mb.autobiography();
			      break;
			      
			case 7:
				System.out.println("Enter your choice:\n 1. Price low to high "
						+ "\n 2.Price high to low \n 3. Best selling");
				int ch = sc.nextInt();

				switch (ch) {

				case 1 : mb.displayByFeature(1);
				         break;
				case 2 : mb.displayByFeature(2); 
				         break;
				case 3 : mb.displayByFeature(3);
						 break;
				}
			
			default:
				System.out.println("Enter valid number");

			}

		}

      }
	
}
