package abc;

public class IllegalArgumentExceptionDemo extends IllegalArgumentException{

	public IllegalArgumentExceptionDemo() {
         super();
	}
	
	public IllegalArgumentExceptionDemo(String s) {
         super(s);
	}

}
