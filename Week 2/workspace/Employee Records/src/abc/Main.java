package abc;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		
		ArrayList<Employee> list = new ArrayList<>();
		
		
		Employee e1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
        list.add(e1);

        Employee e2 = new Employee(2, "Bobby", 22, 500000, "HR", "Bombay");
        list.add(e2);

        Employee e3 = new Employee(3, "Zoe", 20, 750000, "ADMIN", "Delhi");
        list.add(e3);

        Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
        list.add(e4);

        Employee e5 = new Employee(5, "Smitha", 24, 1200000, "HR", "Bengaluru");
        list.add(e5);


        System.out.println(list);
        System.out.println("\n");

        DataStructureA dsa = new DataStructureA();
        dsa.sortingNames(list);
        System.out.println("\n");

        DataStructureB dsb = new DataStructureB();
        dsb.cityNameCount(list);
        System.out.println("\n");

        dsb.monthlySalary(list);
        System.out.println("\n");

	}
	
}
