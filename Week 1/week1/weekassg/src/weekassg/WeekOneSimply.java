package weekassg;

import java.util.Scanner;

class SuperDept{
	public String departmentName() {
		return "Super Department";
	}
	public String work() {
		return "No work as of now";
	}
	public String workDeadline() {
		return "Nil";
	}
	public String holiday() {
		return "Today is not a holiday"; 
	}
	public String techStackInformation() {
		// TODO Auto-generated method stub
		return null;
	}
	public String activity() {
		// TODO Auto-generated method stub
		return null;
	}
}
class AdminDept extends SuperDept{
	public String departmentName() {
		return "Admin Department";
	}
	public String work() {
		return "Complete your documents Submission";
	}
	public String workDeadline() {
		return "Complete by EOD";
	}
}
class HrDept extends SuperDept{
	public String departmentName() {
		return "HR Department";
	}
	public String work() {
		return "Fill todays worksheet and mark your attendance";
	}
	public String workDeadline() {
		return "Complete by EOD";
	}
	public String activity() {
		return "Team Lunch";
	}
}
class TechDept extends SuperDept{
	public String departmentName() {
		return "Tech Department";
	}
	public String work() {
		return "Complete coding of module 1";
	}
	public String workDeadline() {
		return "Complete by EOD";
	}
	public String techStackInformation() {
		return "Core Java";
	}
}

public class WeekOneSimply {

	public static void main(String[] args) {
		
		SuperDept sd = new SuperDept();
		SuperDept ad = new AdminDept();
		SuperDept hd = new HrDept();
		SuperDept td = new TechDept();
		
			System.out.println("Department Name : "+sd.departmentName());
			System.out.println("Work : "+sd.work());
			System.out.println("Work Deadline : "+sd.workDeadline());
			System.out.println("Holiday : "+sd.holiday());
			System.out.println("Department Name : "+ad.departmentName());
			System.out.println("Work : "+ad.work());
			System.out.println("Work Deadline : "+ad.workDeadline());
			System.out.println("Holiday : "+ad.holiday());
			System.out.println("Department Name : "+hd.departmentName());
			System.out.println("Work : "+hd.work());
			System.out.println("Work Deadline : "+hd.workDeadline());
			System.out.println("Activity : "+hd.activity());
			System.out.println("Holiday : "+hd.holiday());
			System.out.println("Department Name : "+td.departmentName());
			System.out.println("Work : "+td.work());
			System.out.println("Work Deadline : "+td.workDeadline());
			System.out.println("Tech Stack Info : "+td.techStackInformation());
			System.out.println("Holiday : "+td.holiday());
		
	}

}
