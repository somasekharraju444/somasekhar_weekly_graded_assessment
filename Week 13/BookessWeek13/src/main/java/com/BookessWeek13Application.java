package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookessWeek13Application {

	public static void main(String[] args) {
		SpringApplication.run(BookessWeek13Application.class, args);
		System.err.println("Server running on 8080");
	}

}
