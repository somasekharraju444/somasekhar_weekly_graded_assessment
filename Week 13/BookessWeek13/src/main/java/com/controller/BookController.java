package com.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Book;
import com.dao.BookRepo;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class BookController {
	
	@Autowired
	BookRepo repo;
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAllBooks(@RequestParam(required = false) String title){
		try {
			List<Book> book =new ArrayList<Book>();
			if(title==null) {
				repo.findAll().forEach(book::add);
			}
			else {
				repo.findByTitleContaining(title).forEach(book::add);
			}
			if(book.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(book,HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable("id") long id){
		Optional<Book> bookData=repo.findById(id);
		if(bookData.isPresent()) {
			return new ResponseEntity<>(bookData.get(),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/books")
	public ResponseEntity<Book> createBook(@RequestBody Book book){
		try {
			Book b =repo.save(new Book(book.getTitle(),book.getDescription(),false));
			
			return new ResponseEntity<>(b,HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
   @PutMapping("/books/{id}")
   public ResponseEntity<Book> updateBook(@PathVariable("id") long id,@RequestBody Book book){
	    
	   Optional<Book> bookData=repo.findById(id);
	   if(bookData.isPresent()) {
		   Book b=bookData.get();
		   b.setTitle(book.getTitle());
		   b.setDescription(book.getDescription());
		   b.setPublished(book.isPublished());
		   return new ResponseEntity<>(repo.save(b),HttpStatus.OK);
	   }else {
		   return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	   }
   }
   @DeleteMapping("/books/{id}")
   public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
     try {
       repo.deleteById(id);
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
     } catch (Exception e) {
       return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
     }
   }

   @DeleteMapping("/books")
   public ResponseEntity<HttpStatus> deleteAllTutorials() {
     try {
       repo.deleteAll();
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
     } catch (Exception e) {
       return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
     }

   }

   @GetMapping("/books/published")
   public ResponseEntity<List<Book>> findByPublished() {
     try {
       List<Book> tutorials = repo.findByPublished(true);

       if (tutorials.isEmpty()) {
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
       }
       return new ResponseEntity<>(tutorials, HttpStatus.OK);
     } catch (Exception e) {
       return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
     }
   }
}
