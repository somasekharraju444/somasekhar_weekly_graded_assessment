package com.surabi.restaurant;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.restaurant.*;

public class Main {
	@SuppressWarnings("deprecation")
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Date time=new Date();
		List<Billing> bills = new ArrayList<Billing>();
		boolean finalOrder;
		List<MenuPreview> items = new ArrayList<MenuPreview>();

		MenuPreview i1 = new MenuPreview(1001, "Biryani", 1, 240.0);
		MenuPreview i2 = new MenuPreview(1002, "Mogulai Chicekn", 1, 215.0);
		MenuPreview i3 = new MenuPreview(1003, "Roti", 8, 80.0);
		MenuPreview i4 = new MenuPreview(1004, "Chicken Lollipop", 6, 180.0);
		MenuPreview i5 = new MenuPreview(1005, "Ice Cream", 1, 120.0);

		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);

		System.out.println("Welcome to Surabi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		while (true) {
			System.out.println("Please Enter the Credentials");
			System.out.println("Email = ");
			String email = sc.next();
			
			System.out.println("Password = ");
			String password = sc.next();
			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);
			
			System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
			String aorU = sc.next();

			Billing bill = new Billing();
			List<MenuPreview> selectedItems = new ArrayList<MenuPreview>();
			
			double totalCost = 0;
			
			Date date=new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (aorU.equals("U") || aorU.equals("u")) {
				System.out.println("Welcome Mr. " + name);
				
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = sc.nextInt();
					
					if (code == 1) {
						selectedItems.add(i1);
						totalCost += i1.getItemPrice();
					}

					else if (code == 2) {
						selectedItems.add(i2);
						totalCost += i2.getItemPrice();
					} else if (code == 3) {
						selectedItems.add(i3);
						totalCost += i3.getItemPrice();
					} else if (code == 4) {
						selectedItems.add(i4);
						totalCost += i4.getItemPrice();
					} else {
						selectedItems.add(i5);
						totalCost += i5.getItemPrice();
					}
					
					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int opt = sc.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);
				
				System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (aorU.equals("A") || aorU.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int option = sc.nextInt();
				switch (option) {
				case 1:
					if ( !bills.isEmpty()) {
						for (Billing b : bills) {
							if(b.getTime().getDate()==time.getDate()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Billing b : bills) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Billing b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (aorU.equals("L") || aorU.equals("l")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry");
			}

		}

	}
}
