package com.restaurant;


import java.time.LocalDateTime;
import java.util.*;

public class Billing {
	
	private String name;
	private List<MenuPreview> items;
	private double cost;
	private Date time;
	
	public Billing() {}
	
	public Billing(String name, List<MenuPreview> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<MenuPreview> getItems() {
		return items;
	}
	public void setItems(List<MenuPreview> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}
	

}
