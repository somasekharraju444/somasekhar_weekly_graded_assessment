package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookessWeek12Application {

	public static void main(String[] args) {
		SpringApplication.run(BookessWeek12Application.class, args);
		System.err.println("Server running on 8080");
	}

}
